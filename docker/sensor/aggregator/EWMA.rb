require "./Aggregator"
class EWMA < Aggregator

    def initialize(sensor_hash, aggregator_hash, syncChannels)
        super
        begin
            addChannel(@aggregator_hash[:instream].to_sym)
            addChannel(@aggregator_hash[:outstream].to_sym)
        rescue Exception => e
            puts "Problems adding a channel: #{e.message}"
            puts e.backtrace.inspect
        end
    end
    def run()
        output=nil
        begin
            gain=@aggregator_hash[:gain]
            inChannel=getChannelByName("instream")
            outChannel=getChannelByName("outstream")
            loop do
                data=inChannel.pop
                puts "EWMA: \tnew data in (#{data})"
                output ||= data
                output = ((output * (gain -1))+data)/gain
                outChannel.push(output)
                puts "EWMA: \tdata out (#{output})"
            end
        rescue Exception => e
            puts "Problems during the run of a publisher: #{e.message}"
            puts e.backtrace.inspect
        end
    end
end
