# coding: utf-8
require 'open-uri'
require 'net/http'
require 'websocket-client-simple'

class Collector

  def wsClient(q,sensor)
    sensorURI="ws://#{sensor}:6909"
    puts "Connecting with #{sensorURI}"
    ws = WebSocket::Client::Simple.connect(sensorURI)
    ws.on :open do
      puts "Websocket with #{ws.url} is opened"
    end
# The following does not seem to work... TODO    
#    ws.on :close do |e|
#      puts "Websocket with #{ws.url} closed (#{e.inspect})"
#      exit 1
#    end
#    ws.on :error do |e|
#      puts "Error on websocket (#{e.inspect})"
#    end
     loop do 
          val=q.pop
          ws.send(val)
          puts "#{val} sent"
     end
  end
  
  def initialize(payload)
    probes = Hash.new # A hash containing all metric probes
    queue = Queue.new # The input queue for measurements
    # Open websocket with sensor         
    sensor=payload[:TargetIP]
    puts "Launch new collector for #{sensor}"
    Thread.new { wsClient(queue,sensor) }
    # Extract collector attributes               
    collector=payload[:attributes][:occi][:collector]
    # Scan mixins and launch probes
    payload[:mixins].each do |mixin|
      attributes=payload[:attributes]
      name=""
      type=""
      # Descend mixin description
      mixin.split("//")[1].split("/").each do |piece|
        piece.split(".").reverse.each do |x|
          name=x.split("#").reverse[0]
          type=x.split("#")[0]
          attributes=attributes[name.to_sym]
        end
      end
      # Launch mixin
      t=Thread.new {
        require "./#{type}/#{name}"
        puts "Launch #{type} #{name}"
        metricProbe=Module.const_get(name)
        metricProbe.new(collector,attributes,queue).run
      }
      puts t.status
      probes[name]=t
    end
    probes.each do |m,t|
      t.join
    end
  end
end

