require 'json'
require 'open-uri'
require 'net/http'
require 'sinatra'
require 'sinatra-websocket'
require './Collector'

set :bind, '0.0.0.0'
set :server, 'thin'
set :port, 6909 #Sinatra port is now standard HTTP
set :sockets, []

syncChannels = {:channels => {}, :lock => Mutex.new }
descr=nil # This will contain the JSON OCCI description of the resource

get '/' do
  if !request.websocket?
    if descr == nil
      response=[202, "Sensor resource is available but undefined"]
    else
      response=[200, descr]
    end
  else
    puts("Request received")
    request.websocket do |ws|
      ws.onopen do 
        puts "Collector connected" 
      end
      ws.onmessage do |msg|
        # includere in nexttick (vedi readme sul sito sinatra websocket)
        h=JSON.parse(msg)
        h.each do |channel,data|
          puts "SENSOR: \tdeliver #{data} to channel #{channel}"
          syncChannels[:lock].synchronize {
            syncChannels[:channels][channel.to_sym].push(data)
          }
        end
      end
      ws.onclose do
        puts "Collector disconnected"
      end
    end
  end
end

post "/collector/:cid" do
  payload = JSON.parse(request.body.read, symbolize_names: true)
  puts "Launching new collector #params['cid']"
  Thread.new {
    Collector.new(payload)
  }
  [200, "OK"]
end

post "/" do
  puts "SENSOR: \tstarting..."
  descr=request.body.read
  payload = JSON.parse(descr, symbolize_names: true)
  syncChannels = {:channels => {}, :lock => Mutex.new }
  plugins = Hash.new # A hash containing all plugins
  # Extract sensor attributes
  sensor=payload[:attributes][:occi][:sensor]
  # Scan mixins and launch plugins
  payload[:mixins].each do |mixin|
    attributes=payload[:attributes]
    name=""
    type=""
    # Descend mixin description
    mixin.split("//")[1].split("/").each do |piece|
      piece.split(".").reverse.each do |x|
        name=x.split("#").reverse[0]
        type=x.split("#")[0]
        attributes=attributes[name.to_sym]
      end
    end
    # Launch mixin
    begin
      require "./#{type}/#{name}"
      plugin=Module.const_get(name)
      puts "Launch #{type} #{name}"
      t=Thread.new {
        plugin.new(sensor,attributes,syncChannels).run
      }
      plugins[name]=t
    rescue Exception => e
      puts "Problems with ./#{type}/#{name}: #{e.message}"
    end
  end
  [200, "OK"] # return code
end
