require "./Metric"

class CPUPercent < Metric
  require "open3"

  def measurement()
    out, err, st = Open3.capture3('w | head -1')
    if st.exitstatus == 0
      perc=out.split("load average:")[1].split(", ")[0].gsub(",",".").to_f*100
    else
      perc=nil
    end
    return JSON.generate(Hash[@metric_hash[:out] => perc])
  end
end
