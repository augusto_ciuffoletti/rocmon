# Rocmon - OCCI compliant monitoring system in Ruby

## Content

This repository contains the code and the files for running the demo sessions

## Files organization

Skip this section if you are not interested in inspecting the code

* The `OCCI-server` directory contains the ruby code for an HTTP server that exposes a less-than-minimal OCCI server. Only operations that are useful for the demo are implemented.

* The `docker` directory contains the code that implements the sensor, the collector, and a generic compute docker. The code is contained in subdirectories with the same name. The build.sh command automatically creates the containers using that code. Note that they are vanilla containers that need to be configured with mixins.

* The `demo0` directory contains the files that describe a very simple monitoring environment made of one compute resource and one sensor.

* The `run.sh` file is used to perform an experiment: it automatically (re-)builds the dockers, launches the OCCI server, and submits to the server the PUT requests to create all the resources indicated in the directory describing the experiment. See below "Make your experiment" section. `Usage: ./run.sh <directory>`

## Running the demo

The `demo0` directory contains three json files that are needed to run the basic demonstration: they describe a compute resource (`g01`), a sensor resource (`s01`), and a collector link (`c01`). The `run.sh` file inside the `demo0` directory is used to display an help message and inspect the operation of the sensor.

To run the demo, in the directory where you find this README file type

	./run.sh demo0

The log on the OCCI server is shown in a pop-up terminal, while in the terminal where you type the command you will see the log of `curl` commands that `PUT` the resource descriptions on the OCCI server. When the `run.sh` command terminates the virtual infrastructure is operational and monitoring is silently running.

To inspect the operation of the virtual infrastructure, use the commands shown on screen when the `run.sh` command terminates. It is easy to grab the monitoring data, or log into the sensor or the compute resource.

## Make your own experiment

To this end you create a new directory (e.g. `test`) and create the JSON files that describe your experiment. There are two rules to follow for filenames:

* the name of the filename corresponds to the id of the entity
* collector names start with a **c**, sensor names starts with a **s**, compute resource names start with a **g** (stands for "generic")

You can add a `run.sh` script if you want to add something special.

## Add a new mixin 

Mixins are in the directories named after the type of the mixin: *metric*, *aggregator*, *publisher*. You can add as many as you want: inspect the examples to understand how to implement one. 

