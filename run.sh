#!/bin/bash

post () {
	i=0
	while ! curl -v --upload-file $1  http://localhost:4567/
	do
		if [[ $i -gt 5 ]]
		then 
			echo -e "\n***\nOCCI server not responding: exiting\n***\n"
			exit 1
		fi
		echo "$i: POST failure ($?)"
		((i++))
		sleep 5
	done
}

if ! [ $1 ]
then
    echo "Usage: $0 <system directory>"
    exit 1
fi

( cd docker; ./build.sh )

xterm -T OCCI-server -e ruby OCCI-Server/OCCI-server.rb &
sleep 2

cd $1
for f in g*; do post $f sleep 1; done
for f in s*; do post $f sleep 1; done
for f in c*; do post $f sleep 1; done
./run.sh
