#/bin/bash

ln -s Dockerfile_generic Dockerfile
docker build -t occi-generic .
rm Dockerfile

ln -s Dockerfile_sensor Dockerfile
docker build -t occi-sensor .
rm Dockerfile
