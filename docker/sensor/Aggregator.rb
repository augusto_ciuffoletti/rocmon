class Aggregator

    def initialize (sensor_hash, aggregator_hash, syncChannels)
        @sensor_hash=sensor_hash
        @aggregator_hash=aggregator_hash
        @syncChannels=syncChannels
    end
    
    def addChannel(name)
      begin
        @syncChannels[:lock].synchronize {
          if !@syncChannels[:channels].has_key?(name.to_sym)
            @syncChannels[:channels][name.to_sym]=Queue.new
          end
        }
#        puts "Create link #{name}"
#        puts @syncChannels[:channels]
      rescue Exception => e
        puts "Problems adding channel #{name}: #{e.message}"
        puts e.backtrace.inspect
      end
    end

    def getChannelByName(name)
      begin
         @syncChannels[:lock].synchronize {
            c=@aggregator_hash[name.to_sym].to_sym
            @syncChannels[:channels][c]
         }
        rescue Exception => e
            puts "Problems in finding the channel: #{e.message}"
        end
    end

#    def getChannelByName(name)
#        begin
#            c=@aggregator_hash[name.to_sym].to_sym
#            @syncChannels[:channels][c]
#        rescue Exception => e
#            puts "Problems in finding the channel: #{e.message}"
#        end
#    end
end
