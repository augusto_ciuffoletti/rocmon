require "./Metric"
class IsReachable < Metric
  require "open3"
    def measurement()
        cmd="ping -c1 -w1 #{@metric_hash[:hostname]}"
        puts cmd
        out, err, st = Open3.capture3(cmd)
        if st.exitstatus == 0
            val=true
        else
            val=false
        end
    return JSON.generate(Hash[@metric_hash[:out] => val])
    end
end
