# coding: utf-8
require 'logger'

class Metric
  def initialize (collector_hash, metric_hash, queue)
#    puts "Initializing "+self.class
    @collector_hash=collector_hash
    @metric_hash=metric_hash
    @queue=queue
  end
  def measurement
    fail NotImplementedError, "No measurement defined for plain metric mixin"
  end
  def run
#    puts Module#name+" running"
    puts @collector_hash
    puts @metric_hash
    loop do
        m=measurement()
        puts "queuing #{m}"
        if defined?(m)
        then
            @queue.push(m)
        else 
            break
        end
        sleep(@collector_hash[:period])
    end
  end
end
