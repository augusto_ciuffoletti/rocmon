require 'json'
require 'sinatra'
require './Collector.rb'

set :bind, '0.0.0.0'
set :server, 'thin'
set :port, 6909 #Sinatra port is now standard HTTP (BAD CHOICE - TODO)
descr=nil # This will contain the JSON OCCI description of the resource
collectors=Hash.new()

get "/" do
  if descr == nil
    [202, "Generic resource is available but undefined"]
  else
    [200, descr]
  end
end

get "/collector/:cid" do
  if collectors[:cid] == nil
    [202, "Collector is available but undefined"]
  else
    [200, collectors[:cid]]
  end
end

post "/collector/:cid" do
  payload = JSON.parse(request.body.read, symbolize_names: true)
  puts "Launching new collector #{payload}"
  Thread.new {
    Collector.new(payload)
  }
  collectors[:cid]=payload
  [200, "OK"]
end

post "/" do
  descr=request.body.read
  [200, "OK"]
end
