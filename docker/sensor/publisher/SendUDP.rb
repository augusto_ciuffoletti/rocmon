# coding: utf-8
require "./Publisher"
require "socket"

class SendUDP < Publisher
  def initialize(sensor_hash, publisher_hash, syncChannels)
    super
    begin
      addChannel(@publisher_hash[:input].to_sym)
      puts "Input channel for SendUDP added"
    rescue Exception => e
      puts "Problems adding a channel: #{e.message}\n"
      puts e.backtrace.inspect
    end
  end
  def run()
    begin
      socket = UDPSocket.new
      inChannel=getChannelByName(:input)
      loop do
        data=inChannel.pop
        puts "UDPSOCKET: \tnew data received (#{data})"
        begin
          socket.send("data="+data.to_s+"\n",0,
                      @publisher_hash[:hostname],
                      @publisher_hash[:port]
                     )
        rescue Exception => e
          puts "Problems sending with UDP(2): #{e.message}"
          puts e.backtrace.inspect
        end
        puts "UDPSOCKET: \tdata sent to socket (#{data})"        
      end
    rescue Exception => e
      puts "Problems sending with UDP: #{e.message}"
      puts e.backtrace.inspect
    end
  end
end
