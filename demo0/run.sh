
echo -e "\n\n
***********************************************\n\
* Two dockers are running: s01, a sensor and  *\n\
* g01, a compute resource.                    *\n\
* - g01 has a collector c01 that monitors     *\n\
*   the CPU load and the connectivity with a  *\n\
*   remote IP                                 *\n\
* - s01 received the measurements from the    *\n\
*   collector and routes the CPU load to a    *\n\
*   local UDP socket, and the true/false      *\n\
*   connectivity output to a logfile          *\n\
* Type the following commands in terminal on  *\n\
* the host:                                   *\n\
*   $ docker exec -i s01 nc -ulp 8888         *\n\
* to attach to the UDP socket;                *\n\
*   $ docker exec -i s01 tail -f /tmp/s01.log *\n\
* to follow the logfile;                      *\n\
*   $ docker logs -f s01                      *\n\
* to see the STDIO of the sensor (and         *\n\
* the same for g01);                          *\n\
*   $ docker exec -it s01 /bin/bash           *\n\
* to attach a terminal to s01 (same for g01)  *\n\
* To kill the dockers type ctrl-C in the      *\n\
* OCCI-server xterm                           *\n\
* Now running -docker logs -f s01 in a term   *\n\
***********************************************"
xterm -sb -si -T "Sensor log" -e docker logs -f s01 &
xterm -sb -si -T "UDP Socket input" -e docker exec -i s01 nc -ulp 8888 &
 
