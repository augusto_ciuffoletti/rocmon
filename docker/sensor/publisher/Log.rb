require "./Publisher"
class Log < Publisher
    def initialize(sensor_hash, publisher_hash, syncChannels)
        super
        begin
            addChannel(@publisher_hash[:in_msg].to_sym)
        rescue Exception => e
            puts "Problemi inserendo un canale: #{e.message}"
            puts e.backtrace.inspect
        end
    end
    def run()
        begin
          inChannel=getChannelByName(:in_msg)
            loop do
              data=inChannel.pop
              puts "LOG: \tnew data in (#{data})"
                begin
                    File.open(@publisher_hash[:filename],"a") do |f|
                        f.puts "#{data}"
                    end
                rescue Exception => e
                    puts "Problem writing to file; #{e.message}"
                end
                puts "LOG: \tdata sent to logfile (#{data})"
            end
        rescue Exception => e
            puts "Problems during the run of a publisher: #{e.message}"
            puts e.backtrace.inspect
        end
            
    end
end
