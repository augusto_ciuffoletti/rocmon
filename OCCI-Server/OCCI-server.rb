# myapp.rb

# Default rocmon port
$port = "6909"
# This is the entities database
$entities = {}
# This is the list of created dockers
$dockers = []
# This MUST precede 'require sinatra' (will execute after sinatra END blocks)
END {
  # terminates and removes all dockers
  $dockers.each do |id|
    puts "Killing docker #{id}"
    `docker rm -f #{id}`
    while ! `docker ps -qf name=#{id}`.empty?
      sleep(1)
      puts "Wait for #{id} do disappear..."
    end
  end
}

require 'sinatra'
require 'rest-client'

# Useful to expose the server on aany network interface
# (by default can be accessed only localhost)
# set :bind, '0.0.0.0'

def getDockerIP(id)
  ip=`docker inspect --format='{{.NetworkSettings.IPAddress}}' #{id}`.chomp
end

def create_link(id)
  descr=JSON.parse($entities[id],symbolize_names:true)
  source=descr[:source]
  # check source is there
  s_ip=getDockerIP(source)
  if s_ip.empty?
    puts "Error: source not yet created"
    halt 404, "Sorry, source of #{id} not yet created\n"
  end
  descr[:SourceIP]=s_ip;
  puts("source #{source} IP address is #{s_ip}")
  # check target is there
  t_ip=getDockerIP(descr[:target])
  if t_ip.empty?
    puts "Error: target not yet created"
    halt 404, "Sorry, target of #{id} not yet created\n"
  end
  descr[:TargetIP]=t_ip;
  begin
    response=RestClient.post("http://#{s_ip}:#{$port}/collector/#{id}", JSON.generate(descr), :content_type => "text+json")
  rescue RestClient::ExceptionWithResponse => err
    puts err.response
    puts "Wait for #{id} collector to be created..."
    sleep(1)
    retry
  end   
  puts "Collector has been created..."
end

def create_resource(id,kid,body)
# This is for the SENSOR and the GENERIC resources
  descr=JSON.parse($entities[id],symbolize_names:true)
  kind=descr[:kind].split("#")[1]
  puts "Now creating #{id}..."
  `docker run --name #{id} -dt occi-#{kind}`
  unless $?.exitstatus == 0
    puts "Cannot create docker for #{id}"
    return 1
  end
  puts status
  $dockers.push(id)
  # check the docker is up
  while `docker ps -qf name=#{id}`.empty?
    sleep(1)
    puts "Wait for #{id} coming up..."
  end
  # retrieves docker IP address
  ip=getDockerIP(id)
  puts("#{id} IP address is #{ip}")
  # Check sinatra is on stage
  begin
    response=RestClient.get("http://#{ip}:#{$port}")
  rescue => err
#    puts err.response
    puts "Wait for #{id} coming up..."
    sleep(1)
    retry #max number of retries TODO
  end
  # Post resource configuration
  begin
    http = RestClient.post("http://#{ip}:#{$port}", $entities[id], :content_type => "text+json")
  rescue  RestClient::ExceptionWithResponse => err
    puts "Error posting #{id} description"
    `docker rm -f #{id}`
    # should remove also from dockers array TODO
    puts err.code
    return
  end
  return ip
end

# SINATRA ROUTES

get '/:id' do |n|
  if $entities.key?(n)
    [200,$entities[n]]
  else
    [404,"#{n} entity not found"]
  end 
end

put '/:fn' do |fn|
  # Request body can be read only once (or rewind)
  body=request.body.read
  begin
    description=JSON.parse(body,symbolize_names:true)
  rescue JSON::ParserError => err
    puts "Error parsing JSON description!"
    puts err.message
    halt 400, "Malformed JSON\n"
  end
  n=description[:id]
  unless fn == n
    puts "Filename should correspond to id: #{fn} stored as #{n}"
  end
  t=description[:kind].split("#")[1]
  puts "#{n} #{t} parsed"
  # Should check format (TODO)
  # Check if entity already exists
  if $entities.key?(n)
    halt 404, "Sorry, #{n} already exists: use POST instead of PUT\n"
  end
  $entities[n]=body
  if t == "collector"
    create_link(n)
  else
    ip=create_resource(n,t,body) # pass only id TODO
    # if ip=undef error
  end
  [201, "Resource created\n"]
end
